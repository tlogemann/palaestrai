# palaestrai -- Full-stack palaestrAI dockerfile
#
# The base image contains the extended Python basis along with the packages
# palaestrAI needs in any case. Derived versions either check out palaestrAI
# or install it from PyPI, depending on the "BUILD_TYPE" argument.
#
# BUILD_TYPE:
#   BUILD_TYPE=development  Check out current development from git
#   BUILD_TYPE=master       Install palaestrai & packages from PyPI
#
# PYTORCH_VERSION:
#   Sets the version of the PyTorch base image as
#   `FROM nvcr.io/nvidia/pytorch:$PYTORCH_VERSION`
#
# BUILD_BASE_TESTING:
#   Base image for the "testing" build type. Defaults to "development", but
#   can also be a completely different base image. Useful for referring to a
#   common base image, such as `palaestrai-base:development`.
#

ARG BUILD_TYPE=development
ARG PYTORCH_VERSION=22.09-py3
ARG BUILD_BASE_MASTER=base
ARG BUILD_BASE_TESTING=base

FROM nvcr.io/nvidia/pytorch:$PYTORCH_VERSION AS base
ENV TZ=UTC
ENV DEBIAN_FRONTEND=noninteractive

RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime \
    && echo "$TZ" > /etc/timezone \
    && apt-get clean \
    && apt-get update \
    && apt-get upgrade -y \
    && apt-get install -y --no-install-recommends \
        wget \
        sudo \
        curl \
        sqlite3 \
        postgresql-client \
        python3-pip \
        build-essential \
        pandoc \
        graphviz \
        libgraphviz-dev \
    && python3 -m pip install -U pip \
    && python3 -m pip install -UI 'llvmlite<0.40,>=0.39.0dev0' \
    && useradd -Um -G users palaestrai \
    && mkdir /palaestrai \
    && mkdir -p /workspace \
    && chown palaestrai:palaestrai /workspace \
    && chmod 1770 /workspace


FROM base AS development
WORKDIR /palaestrai
ENTRYPOINT ["/opt/nvidia/nvidia_entrypoint.sh", "/palaestrai/containers/start.sh"]


FROM $BUILD_BASE_TESTING AS testing
ENTRYPOINT []


FROM $BUILD_BASE_MASTER AS master
ENTRYPOINT ["/opt/nvidia/nvidia_entrypoint.sh", "/palaestrai/containers/start.sh"]


FROM $BUILD_TYPE AS final
WORKDIR /palaestrai
COPY [".", "/palaestrai/"]
RUN \
    python3 -m pip install -U -e . \
    && python3 -m pip install -U '.[full]' \
    && if [ "$BUILD_TYPE" = "development" ]; then \
      python3 -m pip install -U -e '.[full-dev]'; \
    fi \
    && chmod 0755 /palaestrai/containers/start.sh \
    && chown -R palaestrai:users /palaestrai \
    && ls -Al /palaestrai
RUN apt-get autoremove -y \
    && apt-get autoclean \
    && rm -rf /var/lib/apt/lists/* \
    && rm -rf /tmp/{palaestrai*,harl,arsenai,*mosaik*}
WORKDIR /workspace
