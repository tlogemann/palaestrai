"""
This module contains the class :class:`EnvironmentConductor` that
controls the creation of new environments.
"""
from __future__ import annotations

import asyncio
import logging
import signal
import sys
from typing import List, Optional, TYPE_CHECKING
from uuid import uuid4

import aiomultiprocess
from numpy.random import RandomState

from palaestrai.util import seeding, spawn_wrapper
from ..core import BasicState, MajorDomoWorker, RuntimeConfig
from ..core.protocol import (
    EnvironmentSetupRequest,
    EnvironmentSetupResponse,
    ShutdownRequest,
    ShutdownResponse,
)
from ..util.dynaloader import load_with_params

if TYPE_CHECKING:
    from palaestrai.environment import Environment

LOG = logging.getLogger("palaestrai.environment.conductor")


class EnvironmentConductor:
    """The environment conductor creates new environment instances.

    There could be multiple simulation runs and each would need a
    separate environment. The environment conductor controls the
    creation of those new environment instances.

    Parameters
    ----------
    env_cfg : dict
        Dictionary with parameters needed by the environment
    broker_uri : str
        URI used to connect to the simulation broker
    seed : uuid4
        Random seed for recreation
    uid : uuid4
        Unique identifier

    """

    def __init__(self, env_cfg, broker_uri, seed: int, uid=None):
        self._uid: str = uid if uid else "EnvironmentConductor-%s" % uuid4()
        self.seed: int = seed
        self.rng: RandomState = seeding.np_random(self.seed)[0]
        self._environment_configuration = env_cfg
        self._broker_uri = broker_uri
        self._state = BasicState.PRISTINE

        self._tasks: List[aiomultiprocess.Process] = []
        self._worker = None
        self._environment: Optional[Environment] = None

        LOG.debug("%s created.", self)

    @property
    def uid(self) -> str:
        return str(self._uid)

    @property
    def worker(self):
        if not self._worker:
            self._worker = MajorDomoWorker(
                broker_uri=self._broker_uri,
                service=self.uid,
            )
        return self._worker

    def _handle_signal_interrupt(self, signum):
        """Handle interrupting signals by notifying of the state change."""
        LOG.info(
            "EnvironmentConductor(id=0x%x, uid=%s) interrupted by signal %s.",
            id(self),
            self.uid,
            signum,
        )
        self._state = {
            signal.SIGINT.value: BasicState.SIGINT,
            signal.SIGABRT.value: BasicState.SIGABRT,
            signal.SIGTERM.value: BasicState.SIGTERM,
        }[signum]

    def _init_signal_handler(self):
        """Sets handlers for interrupting signals in the event loop."""
        signals = {signal.SIGINT, signal.SIGABRT, signal.SIGTERM}
        LOG.debug(
            "EnvironmentConductor(id=0x%x, uid=%s) "
            "registering signal handlers for signals %s.",
            id(self),
            self.uid,
            signals,
        )
        loop = asyncio.get_running_loop()
        for signum in signals:
            loop.add_signal_handler(
                signum, self._handle_signal_interrupt, signum
            )

    async def _monitor_state(self):
        known_state = self._state
        while known_state.value == self._state.value:
            try:
                await asyncio.sleep(0.5)
            except asyncio.CancelledError:
                break
        LOG.debug(
            "EnvironmentConductor(id=0x%x, uid=%s) "
            "state changed from %s to %s.",
            id(self),
            self.uid,
            known_state,
            self._state,
        )

    def _load_environment(self):
        """Loads the ::`Environment` and necessary dependent classes."""
        env_name = None
        env_params = {}
        try:
            env_name = self._environment_configuration["environment"]["name"]
            env_uid = self._environment_configuration["environment"].get(
                "uid", f"Environment-{uuid4()}"
            )
            env_params = self._environment_configuration["environment"][
                "params"
            ]
        except KeyError:
            LOG.critical(
                "%s could not load environment: Configuration not present. "
                'Key "environment" is missing in environment configuration. '
                "The configuration currently contains: %s",
                self,
                self._environment_configuration,
            )
            raise
        env_params.update(
            {
                "uid": env_uid,
                "broker_uri": self._broker_uri,
                "seed": self.rng.randint(0, sys.maxsize),
            }
        )

        LOG.debug(
            "%s loading Environment '%s' with params '%s'.",
            self,
            env_name,
            env_params,
        )
        try:
            self._environment = load_with_params(env_name, env_params)
        except ValueError as e:
            LOG.critical(
                "%s could not load environment '%s': %s. Perhaps a typo in "
                "your configuration? %s",
                self,
                env_name,
                e,
                self._environment_configuration["environment"],
            )
            raise e

        if "state_transformer" in self._environment_configuration:
            self._environment._state_transformer = load_with_params(
                self._environment_configuration["state_transformer"]["name"],
                self._environment_configuration["state_transformer"]["params"],
            )
            LOG.debug(
                "%s loaded %s for %s",
                self,
                self._environment._state_transformer,
                self._environment,
            )
        if "reward" in self._environment_configuration:
            self._environment.reward = load_with_params(
                self._environment_configuration["reward"]["name"],
                self._environment_configuration["reward"]["params"],
            )
            LOG.debug(
                "%s loaded %s for %s",
                self,
                self._environment.reward,
                self._environment,
            )

    def _init_environment(self):
        """Initialize a new environment.

        Creates a new environment instance with its own UID.

        Returns
        -------
        str
            The unique identifier of the new environment
        """
        try:
            env_process = aiomultiprocess.Process(
                name=f"Environment-{self.uid}",
                target=spawn_wrapper,
                args=(
                    f"Environment-{self.uid}",
                    RuntimeConfig().to_dict(),
                    self._environment.run,
                ),
            )
            env_process.start()
            self._tasks.append(env_process)
        except Exception as e:
            LOG.critical(
                "%s encountered a fatal error while executing %s: %s. "
                "Judgement day is nigh!",
                self,
                self._environment,
                e,
            )
            raise e
        return self._environment.uid

    async def run(self):
        self._init_signal_handler()
        self._state = BasicState.RUNNING
        request = None
        reply = None
        LOG.info("%s commencing run: creating better worlds.", self)

        state_monitor_task = asyncio.create_task(self._monitor_state())
        while self._state == BasicState.RUNNING:
            transceive_task = asyncio.create_task(
                self.worker.transceive(reply)
            )
            tasks_done, tasks_pending = await asyncio.wait(
                {state_monitor_task, transceive_task},
                return_when=asyncio.FIRST_COMPLETED,
            )
            if transceive_task in tasks_pending:
                continue
            request = transceive_task.result()
            if not request:
                continue

            LOG.info(
                "EnvironmentConductor(id=0x%x, uid=%s) "
                "received request: %s(%s).",
                id(self),
                self.uid,
                request.__class__,
                request.__dict__,
            )

            if isinstance(request, EnvironmentSetupRequest):
                LOG.debug(
                    "EnvironmentConductor(id=0x%x, uid=%s) "
                    "received EnvironmentSetupRequest(experiment_run_id=%s).",
                    id(self),
                    self.uid,
                    request.experiment_run_id,
                )
                self._load_environment()
                LOG.info(
                    "%s loaded %s, starting subprocess...",
                    self,
                    self._environment,
                )
                env_uid = self._init_environment()
                ssci = request.sender_simulation_controller_id
                reply = EnvironmentSetupResponse(
                    sender_environment_conductor=self.uid,
                    receiver_simulation_controller=ssci,
                    environment_id=env_uid,
                    experiment_run_id=request.experiment_run_id,
                    experiment_run_instance_id=request.experiment_run_instance_id,
                    experiment_run_phase=request.experiment_run_phase,
                    environment_type=self._environment_configuration[
                        "environment"
                    ]["name"],
                    environment_parameters=self._environment_configuration[
                        "environment"
                    ].get("params", dict()),
                )
            if isinstance(request, ShutdownRequest):
                self._state = BasicState.STOPPING
                for task in self._tasks:
                    await task.join()

        reply = ShutdownResponse(
            request.experiment_run_id if request else None
        )
        await self.worker.transceive(reply, skip_recv=True)
        self._state = BasicState.FINISHED
        LOG.info(
            "EnvironmentConductor(id=0x%x, uid=%s) completed shutdown.",
            id(self),
            self.uid,
        )

    def __str__(self):
        return f"{self.__class__.__name__}(id=0x{id(self):x}, uid={self.uid})"
