"""This module contains the class :class:`AgentConductor` that
stores all information the agents need about an actuator.

"""
from __future__ import annotations

from collections import namedtuple
from copy import deepcopy
import warnings

import aiomultiprocess
import asyncio
import logging
import setproctitle
import signal
import socket
import uuid
from typing import List, Optional
from uuid import uuid4

from palaestrai.core import MajorDomoWorker, RuntimeConfig
from palaestrai.core.protocol import (
    AgentSetupRequest,
    AgentSetupResponse,
    ShutdownRequest,
    ShutdownResponse,
)
from palaestrai.util import spawn_wrapper
from palaestrai.util.dynaloader import load_with_params, ErrorDuringImport
from palaestrai.util.exception import TasksNotFinishedError
from .brain import Brain
from .brain_dumper import BrainDumper, BrainLocation
from .muscle import Muscle

LOG = logging.getLogger(__name__)


ExperimentRunInfo = namedtuple(
    "ExperimentRunInfo",
    ["experiment_run_uid", "experiment_run_phase"],
    defaults=(None, None),
)


async def _run_brain(agent_conductor_uid: str, brain: Brain):
    """Executes the Brain main loop, handling signals and errors

    This method wraps :py:func:`Brain.run`. It takes care of proper
    installment of signal handlers, setting of the proctitle, and most
    importantly, error handling. This method should be wrapped in the
    :py:func:`palaestrai.util.spawn_wrapper` function, which, in turn, is the
    target of an :py:func:`aiomultiprocess.Process.run` call.

    Parameters
    ----------
    agent_conductor_uid : str
        UID of the governing ::`~AgentConductor`
    brain : Brain
        The ::`~Brain` instance that should be run.

    Returns
    -------
    Any
        Whatever the ::`~Brain.run` method returns.
    """
    signal.signal(signal.SIGINT, signal.SIG_DFL)
    signal.signal(signal.SIGABRT, signal.SIG_DFL)
    signal.signal(signal.SIGTERM, signal.SIG_DFL)
    setproctitle.setproctitle(
        "palaestrAI[Brain-%s]" % agent_conductor_uid[-6:]
    )
    try:
        return await brain.run()
    except Exception as e:
        LOG.critical(
            "Brain(id=0x%x, agent_conductor_uid=%s) died from a fatal wound "
            "to the head: %s",
            id(brain),
            agent_conductor_uid,
            e,
        )
        raise


async def _run_muscle(muscle: Muscle):
    """Executes the ::`~Muscle` main loop, handling signals and errors

    This method wraps :py:func:`Muscle.run`. It takes care of proper
    installment of signal handlers, setting of the proctitle, and most
    importantly, error handling. This method should be wrapped in the
    :py:func:`palaestrai.util.spawn_wrapper` function, which, in turn, is the
    target of an :py:func:`aiomultiprocess.Process.run` call.

    Parameters
    ----------
    muscle : Muscle
        The  ::`~Muscle` instance that runs

    Returns
    -------
    Any
        Whatever ::`~Muscle.run` returns
    """
    signal.signal(signal.SIGINT, signal.SIG_DFL)
    signal.signal(signal.SIGABRT, signal.SIG_DFL)
    signal.signal(signal.SIGTERM, signal.SIG_DFL)
    setproctitle.setproctitle("palaestrAI[Muscle-%s]" % muscle.uid[-6:])
    try:
        return await muscle.run()
    except Exception as e:
        LOG.critical(
            "Muscle(id=0x%x, uid=%s) suffers from dystrophy: %s",
            id(muscle),
            muscle.uid,
            e,
        )
        raise


class AgentConductor:
    """This creates a new agent conductor (AC).

    The AC receives an agent config, which contains all information for
    the brain and the muscle. Additional information, like the current
    run ID, are part of the AgentSetupRequest.

    Parameters
    ----------
    broker_uri: str
        Connection information, should be in the format
        protocol://ip-address:port, e.g., "tcp://127.0.0.1:45310"
    agent_config: dict
        A *dict* containing information, how to instantiate brain and
        muscle.
    seed: int
        The random seed for this agent conductor.
    uid : str
        The uid (a unique string) for this agent conductor object.

    Attributes
    ----------
    _muscles: List[str]
        A list containing the uids of all the muscles.
    _brain: :class:`palaestrai.agent.Brain`:
        A reference to the brain instance of this agent conductor.
    """

    def __init__(
        self,
        broker_uri: str,
        agent_config: dict,
        seed: int,
        uid=None,
    ):
        self._seed = seed
        self._config = agent_config
        self._uid = str(uid) if uid else "AgentConductor-%s" % uuid4()

        self._brain: Optional[Brain] = None
        self._brain_uri: Optional[str] = None
        self._muscles: List[uuid.UUID] = []

        self._worker = None
        self._brain_process = None
        self._broker_uri = broker_uri
        self._experiment_info: Optional[ExperimentRunInfo] = None
        self.tasks: List[aiomultiprocess.Process] = []

    def _handle_sigintterm(self, signum, frame):
        LOG.info(
            "AgentConductor(id=0x%x, uid=%s) "
            "interrupted by signal %s in frame %s",
            id(self),
            self.uid,
            signum,
            frame,
        )
        raise SystemExit()

    @property
    def uid(self):
        """Unique, opaque ID of the agent conductor object"""
        return self._uid

    @property
    def worker(self):
        """Getter for the :py:class:`MajorDomoWorker` object

        This method returns (possibly lazily creating) the current
        :py:class:`MajorDomoWorker` object. It creates this worker on demand.
        It is not safe to call this method between forks, as forks copy
        the context information for the worker which is process-depentent.

        :rtype: MajorDomoWorker
        """
        if self._worker is None:
            self._worker = MajorDomoWorker(self._broker_uri, self.uid)
        return self._worker

    def _load_brain(self, actuators, sensors, sock) -> Brain:
        # deepcopy, or we'd modify the original. Not what we want:
        try:
            params = deepcopy(self._config["brain"]["params"])
        except KeyError:
            params = {}
        try:
            objective = load_with_params(
                self._config["objective"]["name"],
                self._config["objective"].get("params", {}),
            )
        except (ModuleNotFoundError, ErrorDuringImport, AttributeError) as e:
            LOG.critical("%s could not load objective: %s, aborting", self, e)
            raise
        params.update(
            {
                "seed": self._seed,
                "sensors": sensors,
                "actuators": actuators,
                "objective": objective,
                "muscle_updates_listen_uri_or_socket": sock,
            }
        )
        try:
            brain: Brain = load_with_params(
                self._config["brain"]["name"], params
            )
        except (ModuleNotFoundError, ErrorDuringImport, AttributeError) as e:
            LOG.critical("%s could not load Brain: %s, aborting", self, e)
            raise
        brain._uid = f"{self.uid}.Brain-{str(uuid4())}"
        brain._dumpers = self._load_brain_dumpers()
        return brain

    def _init_brain(self, sensors, actuators):
        """Initialize the brain process.

        Each agent, which is represented by an individual conductor,
        has one brain process. This function initializes the brain
        process.

        The agent conductor allocates the port for the brain-muscle
        interconnection. For this, it binds to a random port given from the OS.
        It passes the port to the brain and closes the socket; the Brain will
        then re-open the socket as ZMQ socket. That works because sockets are
        refcounted and the ref count goes to 0 when the ::`Brain` closes the
        socket before re-opening it. The agent conductor then uses the port
        number (not the socket itself) to pass it to the ::`Muscle` objects,
        which then know where to find their ::`Brain`.

        Parameters
        ----------
        sensors : List[SensorInformation]
            List of available sensors.
        actuators : List[ActuatorInformation]
            List of available actuators.

        Returns
        -------
        str
            The listen URI of the brain.
        """

        # We create a simple socket first in order to get a free port from the
        # OS. Its only an ephermal server, we close the port as soon as the
        # Brain process is started.

        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock.bind(("", 0))
        listen_port = sock.getsockname()[1]

        self._brain = self._load_brain(actuators, sensors, sock)
        try:
            self._brain_process = aiomultiprocess.Process(
                name=f"Brain-{self.uid}",
                target=spawn_wrapper,
                args=(
                    f"Brain-{self.uid}",
                    RuntimeConfig().to_dict(),
                    _run_brain,
                    [self.uid, self._brain],
                ),
            )
            self._brain_process.start()
            sock.close()

            LOG.debug(
                "AgentConductor(id=0x%x, uid=%s) started Brain(id=0x%x)",
                id(self),
                self.uid,
                id(self._brain),
            )
        except Exception as e:
            LOG.critical(
                "AgentConductor(id=0x%x, uid=%s) "
                "encountered a fatal error while executing "
                "Brain(id=0x%x): %s",
                id(self),
                self.uid,
                id(self._brain),
                e,
            )
            raise
        return "tcp://%s:%s" % (
            "*" if RuntimeConfig().public_bind else "127.0.0.1",
            listen_port,
        )

    def _init_muscle(self, uid, brain_uri):
        """Function to initialize a new muscle

        Each agent consists of one brain and at least one muscle
        but is not limited to one muscle. There can be multiple
        muscles and muscles can be restarted.

        Parameters
        ----------
        uid : uuid4
            Unique identifier to identify the muscle
        brain_uri : str
            URI string designating the ::`Brain` listening socket to connect
            a ::`Muscle` to.
        """
        try:
            params = deepcopy(self._config["muscle"]["params"])
        except KeyError:
            params = {}

        params.update(
            {
                "uid": uid,
                "brain_uri": brain_uri,
                "brain_id": id(self._brain),
                "broker_uri": self._broker_uri,
            }
        )
        muscle = load_with_params(self._config["muscle"]["name"], params)
        muscle._brain_loaders = self._load_brain_dumpers()
        muscle.setup()
        self._muscles.append(uid)
        try:
            agent_process = aiomultiprocess.Process(
                name=f"Muscle-{uid}",
                target=spawn_wrapper,
                args=(
                    f"Muscle-{uid}",
                    RuntimeConfig().to_dict(),
                    _run_muscle,
                    [muscle],
                ),
            )
            agent_process.start()
            LOG.debug(
                "AgentConductor(id=0x%x, uid=%s) "
                "started Muscle(id=0x%x, uid=%s).",
                id(self),
                self.uid,
                id(muscle),
                muscle.uid,
            )
            self.tasks.append(agent_process)
        except Exception as e:
            LOG.critical(
                "AgentConductor(id=0x%x, uid=%s) "
                "encountered a fatal error while executing "
                "Muscle(id=0x%x, uid=%s): %s",
                id(self),
                self.uid,
                id(muscle),
                muscle.uid,
                e,
            )
            raise e

    async def run(self):
        """Monitors agents and facilitates information interchange

        This method is the main loop for the :py:class:`AgentConductor`. It
        monitors the :py:class:`Brain` object and :py:class:`Muscle` instances
        of the agent (i.e., the processes) and transceives/routes messages.

        """
        signal.signal(signal.SIGINT, signal.SIG_DFL)
        signal.signal(signal.SIGABRT, signal.SIG_DFL)
        signal.signal(signal.SIGTERM, signal.SIG_DFL)
        setproctitle.setproctitle(
            "palaestrAI[AgentConductor-%s]" % self._uid[-6:]
        )

        signal.signal(signal.SIGINT, self._handle_sigintterm)
        signal.signal(signal.SIGTERM, self._handle_sigintterm)
        LOG.info(
            "AgentConductor(id=0x%x, uid=%s) commencing run: "
            "Today's solutions to tomorrow's problems",
            id(self),
            self.uid,
        )
        proceed = True
        request = None
        reply = None
        while proceed:
            try:
                request = await self._housekeeping(reply)
            except TasksNotFinishedError:
                continue
            except SystemExit:
                break

            if isinstance(request, AgentSetupRequest):
                reply = self._handle_agent_setup(request)

            if isinstance(request, ShutdownRequest):
                await self._handle_shutdown(request)
                break

        LOG.debug(
            "AgentConductor(id=0x%x, uid=%s) sending "
            "ShutdownResponse(experiment_run_id=%s)",
            id(self),
            self.uid,
            request.experiment_run_id,
        )
        reply = ShutdownResponse(request.experiment_run_id)
        try:
            await self.worker.transceive(reply, skip_recv=True)
        except SystemExit:
            pass  # If they really want to, we can skip that, too.
        LOG.info(
            "AgentConductor(id=0x%x, uid=%s) completed shutdown: "
            "ICH, AC, BIN NUN TOD, ADJÖ [sic].",
            id(self),
            self.uid,
        )

    async def _housekeeping(self, reply):
        """Keep the household clean and lively.

        In this method, replies are send and requests are received.
        Furthermore, the AC sees over his child tasks (muscles and
        brain).

        Parameters
        ----------
        reply:
            The next reply to send

        Returns
        -------
        request
            The request received during transceiving.

        """
        LOG.debug(
            "AgentConductor(id=0x%x, uid=%s) starts housekeeping. "
            "Everything needs to be in proper order.",
            id(self),
            self.uid,
        )
        try:
            transceive_task = asyncio.create_task(
                self.worker.transceive(reply)
            )
            muscle_tasks = [asyncio.create_task(p.join()) for p in self.tasks]
            brain_tasks = (
                [asyncio.create_task(self._brain_process.join())]
                if self._brain_process
                else []
            )
            tasks_done, tasks_pending = await asyncio.wait(
                [transceive_task] + muscle_tasks + brain_tasks,
                return_when=asyncio.FIRST_COMPLETED,
            )
            if not tasks_done:
                # This shouldn't happen, but you never know.
                raise TasksNotFinishedError()

            terminated_workers = [
                p
                for p in self.tasks + [self._brain_process]
                if p is not None and not p.is_alive() and p.exitcode != 0
            ]
            if terminated_workers:
                # I don't think the other tasks should end like this?
                LOG.critical(
                    "AgentConductor(id=0x%x, uid=%s) "
                    "has suffered from prematurely dead tasks: %s",
                    id(self),
                    self.uid,
                    [p.name for p in terminated_workers],
                )
                signal.signal(signal.SIGINT, signal.SIG_DFL)
                signal.signal(signal.SIGABRT, signal.SIG_DFL)
                signal.signal(signal.SIGTERM, signal.SIG_DFL)
                raise RuntimeError(
                    "AgentConductor(id=0x%x, uid=%s) "
                    "has dead tasks at hands: %s"
                    % (
                        id(self),
                        self.uid,
                        [p.name for p in terminated_workers],
                    )
                )
            if transceive_task not in tasks_done:
                await transceive_task
            request = transceive_task.result()
        except SystemExit as e:
            LOG.warning(
                "AgentConductor(id=0x%x, uid=%s) "
                "stopping due to SIGINT/SIGTERM",
                id(self),
                self.uid,
            )
            raise e

        LOG.debug(
            "AgentConductor(id=0x%x, uid=%s) got a %s. "
            "Let's see how we handle this one.",
            id(self),
            self.uid,
            request,
        )

        return request

    def _handle_agent_setup(self, request: AgentSetupRequest):
        """Handle the agent setup request.

        One setup request will result in one new muscle created.
        The brain will be created if necessary.

        Parameters
        ----------
        request: :class:`.AgentSetupRequest`
            The agent setup request with information for the muscle to
            be created.

        Returns
        -------
        :class:`.AgentSetupResponse`
            The response for the simulation controller.

        """
        if request.receiver_agent_conductor == self.uid:
            self._experiment_info = ExperimentRunInfo(
                experiment_run_uid=request.experiment_run_id,
                experiment_run_phase=request.experiment_run_phase,
            )

            if self._brain is None:
                LOG.debug("Sensors: " + str(request.sensors)
                          + "\n Actuator: " + str(request.actuators))
                self._brain_uri = self._init_brain(
                    request.sensors, request.actuators
                )
            self._init_muscle(request.agent_id, self._brain_uri)

            return AgentSetupResponse(
                sender_agent_conductor=self.uid,
                receiver_simulation_controller=request.sender,
                experiment_run_id=request.experiment_run_id,
                experiment_run_instance_id=request.experiment_run_instance_id,
                experiment_run_phase=request.experiment_run_phase,
                agent_id=request.agent_id,
            )

    def _load_brain_dumpers(self):
        """Loads all ::`~BrainDumper` descendants

        Through introspection, all classes that are descendants of
        ::`~BrainDumper` will be loaded. They have to be imported here in
        order for this to work.
        """

        dumpers = []
        for subclazz in BrainDumper.__subclasses__():
            try:
                current_location = BrainLocation(
                    agent_name=self._config["name"],
                    experiment_run_uid=self._experiment_info.experiment_run_uid,
                    experiment_run_phase=self._experiment_info.experiment_run_phase,
                )

                lcfg = {
                    "agent": self.uid,
                    "experiment_run": self._experiment_info.experiment_run_uid,
                    "phase": max(
                        0, self._experiment_info.experiment_run_phase - 1
                    ),
                }
                user_cfg = self._config.get("load", {})
                if not isinstance(user_cfg, dict):
                    warnings.warn(
                        f"{str(self)} received malformed `load` configuration:"
                        f" {user_cfg}. Continueing with defaults.",
                        UserWarning,
                    )
                    user_cfg = {}

                lcfg.update(user_cfg)
                previous_location = BrainLocation(
                    agent_name=lcfg["agent"],
                    experiment_run_uid=lcfg["experiment_run"],
                    experiment_run_phase=lcfg["phase"],
                )

                obj = subclazz(
                    dump_to=current_location, load_from=previous_location
                )
                dumpers.append(obj)
            except TypeError as e:
                LOG.warning(
                    "%s could not register brain dumper %s: %s, skipping",
                    self,
                    subclazz,
                    e,
                )
        LOG.debug("%s loaded %d dumpers: %s", self, len(dumpers), dumpers)
        return dumpers

    async def _handle_shutdown(self, request):
        """Handle the shutdown request for this agent conductor.

        It is expected that all muscles and the brain of this
        agent conductor already received a shutdown request. Therefore,
        all this method does is to wait(join) for the processes.

        Parameters
        ----------
        request: :class:`.ShutdownRequest`
            The shutdown request
        """
        for task in self.tasks:
            await task.join()
        await self._brain_process.join()

    def __str__(self):
        return f"<AgentConductor(uid={self.uid})"
