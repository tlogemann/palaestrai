from __future__ import annotations

from typing import TYPE_CHECKING, List

from palaestrai.agent.memory import Memory
from palaestrai.agent.objective import Objective

if TYPE_CHECKING:
    from palaestrai.agent import RewardInformation


class DummyObjective(Objective):
    def __init__(self, params):
        super().__init__(params)

    def internal_reward(self, memory: Memory, **kwargs) -> float:
        final_rewards = [r[1] for r in memory.rewards.iterrows()]
        return sum(final_rewards)
