"""This module contains the abstract class :class:`Brain` that is used
to implement the thinking part of agents.

"""

from __future__ import annotations

import asyncio
import logging
import pickle
import queue
import signal
import socket
import time
import uuid
import warnings
import zlib
from abc import ABC, abstractmethod
from pathlib import Path
from threading import Thread
from typing import TYPE_CHECKING, List, Union, Optional

import numpy as np
import zmq
from zmq import ZMQError

from palaestrai.core import RuntimeConfig
from palaestrai.core.protocol import (
    MuscleUpdateRequest,
    MuscleUpdateResponse,
    MuscleShutdownRequest,
    MuscleShutdownResponse,
)
from palaestrai.util.exception import BrainMuscleConnectionFailedError
from .brain_dumper import BrainDumper
from .memory import Memory
from .state import State
from ..util.dynaloader import load_with_params

LOG = logging.getLogger(__name__)

if TYPE_CHECKING:
    from . import SensorInformation, ActuatorInformation, Objective


def _load_memory(
    memory_params: dict, memory_path: str = "palaestrai.agent.memory:Memory"
) -> Memory:
    try:
        memory = load_with_params(memory_path, memory_params)
    except Exception as e:
        LOG.error(
            "Could not load memory from path %s with params %s",
            memory_path,
            memory_params,
        )
        raise e
    return memory


class Brain(ABC):
    """Baseclass for all brain implementation

    The brain is the central learning instance. It coordinates all
    muscles (if multiple muscles are available). The brain does all
    (deep) learning tasks and delivers a model to the muscles.

    The brain has one abstract method :meth:`.thinking` that has to be
    implemented.

    Brain objects store their state and can re-load previous states by using
    the infrastructure provided by the :class:`~BrainDumper` infrastructure.
    For this, concrete Brain classes need to provide implementations of
    :meth:`~Brain.load` and :meth:`~Brain.store`.

    Parameters
    ----------

    muscle_updates_listen_uri : str
        The IP and port on which the brain should bind, so that the
        muscles can connect to the brain, e.g: ``tcp://127.0.0.1:1234``.
    sensors : List[SensorInformation]
        A *list* of available sensors, can be used to define, e.g., the
        input space of a neural network.
    actuators : List[ActuatorInformation]
        A *list* of available actuators, can be used to define, e.g.,
        the output space of a neural network.
    objective : :class:`~Objective`
        The objective object that expresses the goal of an agent with regards
        to the reward it receives
    seed : int
        Random seed for all random distributions used by the Brain
    """

    def __init__(
        self,
        muscle_updates_listen_uri_or_socket: Union[str, socket.socket],
        sensors: List[SensorInformation],
        actuators: List[ActuatorInformation],
        objective: Objective,
        seed: int,
        **params,
    ):
        self._seed: int = seed
        self._state = State.PRISTINE
        self._uid: str = f"Brain-{uuid.uuid4()}"

        self.sensors = sensors
        self.actuators = actuators
        self.objective = objective

        self._ctx = None
        self._memory = None
        self._params = params
        self._muscle_updates_socket: Optional[
            Union[socket.socket, zmq.Socket]
        ] = None
        self._muscle_updates_listen_uri: Optional[str] = None
        if isinstance(muscle_updates_listen_uri_or_socket, socket.socket):
            self._muscle_updates_socket = muscle_updates_listen_uri_or_socket
            self._muscle_updates_listen_uri = "tcp://%s:%s" % (
                "*" if RuntimeConfig().public_bind else "127.0.0.1",
                self._muscle_updates_socket.getsockname()[1],
            )
        else:
            self._muscle_updates_listen_uri = (
                muscle_updates_listen_uri_or_socket
            )
        self._worker_updates: Optional[queue.Queue] = None

        # Some IO object to which we can dump ourselves for a freeze:
        self._dumpers: List[BrainDumper] = list()

    @property
    def seed(self) -> int:
        """Returns the random seed applicable for this brain instance."""
        return self._seed

    @property
    def state(self) -> State:
        return self._state

    @property
    def _context(self):
        if self._ctx is None:
            self._ctx = zmq.Context()
        return self._ctx

    @property
    def _muscle_updates_queue(self):
        if not self._worker_updates:
            self._worker_updates = queue.Queue()
        return self._worker_updates

    def _handle_sigintterm(self, signum, frame):
        self._state = State.CANCELLED
        LOG.warning(
            "Brain(id=0x%x) interrupted by signal %s in frame %s.",
            id(self),
            signum,
            frame,
        )

    def _listen(self):
        """Creates the muscle updates listen socket"""
        if isinstance(self._muscle_updates_socket, socket.socket):
            self._muscle_updates_socket.close()
        self._muscle_updates_socket = self._context.socket(zmq.ROUTER)
        retrys = 0
        while retrys < 20:
            try:
                self._muscle_updates_socket.bind(
                    self._muscle_updates_listen_uri
                )
                break
            except ZMQError:
                time.sleep(retrys * 0.01)
                retrys += 1
        if retrys == 20:
            LOG.critical(
                "%s was unable to connect to the muscle communication port.",
                self,
                self._muscle_updates_listen_uri,
            )
            raise BrainMuscleConnectionFailedError

        LOG.debug(
            "%s bound to %s",
            self,
            self._muscle_updates_listen_uri,
        )

    def _receive_updates(self):
        """Receives updates from workers and stores them in the queue."""
        self._listen()
        while self.state == State.RUNNING:
            try:
                z = self._muscle_updates_socket.recv_multipart(
                    flags=zmq.NOBLOCK
                )
            except zmq.ZMQError:
                time.sleep(0.05)
                continue
            p = zlib.decompress(z[1])
            msg = pickle.loads(p)
            if msg is not None:
                self._muscle_updates_queue.put([msg, z[0]])
            if isinstance(msg, MuscleShutdownRequest):
                self._state = State.STOPPING
        LOG.debug("Brain(id=0x%x) has stopped receiver.", id(self))

    def _send(self, message, header, flags=0, protocol=-1):
        """Send a message to a muscle.

        Parameters
        ----------
        message : MuscleUpdateResponse
            The message that should be send as response.
        header : str or bytes
            The ID of the receiving muscle.
        flags : int, optional
            ZeroMQ-Flags which should be used when sending.
        protocol : int, optional
            Protocol used by pickle.dumps

        """
        p = pickle.dumps(message, protocol)
        z = zlib.compress(p)
        self._muscle_updates_socket.send_multipart(
            [bytes(header), z], flags=flags
        )

    async def run(self):
        """Start the brain main loop.

        This method starts the brain, it begins to listen for messages
        and as soon as messages are arriving, it processes those
        messages by calling the thinking methode. This will return a
        Muscleupdateresponse, which is sent back to the muscle.
        """
        if any(d for d in self._dumpers if d._brain_source):
            try:
                self.load()
            except AttributeError:
                # This happens because somebody forgot to check whether loader
                # returned None...
                LOG.error(
                    "%s tried to load a brain dump, but that seemed to have "
                    "failed. However, instead of a sane reboot, the brain "
                    "never checked whether the loader returned 'None'. So, "
                    "the loading failed completely. Don't blame me, blame the "
                    "implementor. My brain will go on, but don't expect a "
                    "happy-end from me.",
                    self,
                )
        self._state = State.RUNNING
        signal.signal(signal.SIGINT, self._handle_sigintterm)
        signal.signal(signal.SIGTERM, self._handle_sigintterm)
        receiver_thread = Thread(target=self._receive_updates)
        receiver_thread.start()
        LOG.info(
            "%s started: complex ideas will now become real.",
            self,
        )

        while (
            self.state == State.RUNNING
            or not self._muscle_updates_queue.empty()
        ):
            try:
                msg = self._muscle_updates_queue.get(timeout=1)
            except TimeoutError:
                continue
            except queue.Empty:
                await asyncio.sleep(0.05)
                continue
            msg, header = msg[0], msg[1]
            if isinstance(msg, MuscleUpdateRequest):
                LOG.debug(
                    "%s will think about that breaking new %s "
                    "that just arrived.",
                    self,
                    msg,
                )
                if self._memory is None:
                    self._memory = _load_memory(
                        memory_path=self._params.get(
                            "memory_class", "palaestrai.agent.memory:Memory"
                        ),
                        memory_params=self._params.get(
                            "memory_params",
                            {
                                "rewards": msg.reward,
                                "observations": msg.sensors_available,
                                "actions": msg.actuators_available,
                            },
                        ),
                    )

                try:
                    self._memory.append(
                        msg.reward,
                        msg.actuators_available,
                        msg.sensors_available,
                        msg.additional_data,
                    )
                    internal_reward = self.objective.internal_reward(
                        self._memory
                    )
                    if (
                        internal_reward is None
                        or np.any(np.isnan(internal_reward))
                        or np.any(np.isinf(internal_reward))
                    ):
                        LOG.warning(
                            "%s got a strange internal reward. "
                            "you might be in for some strange errors "
                            "coming from your Brain or Muscle."
                            "Result of %s.internal_reward(%s) was: %s",
                            self,
                            self.objective.__class__,
                            msg.reward,
                            internal_reward,
                        )
                    self._memory.append_internal_rewards(internal_reward)
                except Exception as e:  # User code. Could by anything, really
                    LOG.exception(
                        "%s tried to calculate its objective, "
                        "but failed: %s. Unrecoverable; raising error",
                        self,
                        e,
                    )
                    raise

                response = self.thinking(
                    header,
                    msg.previous_network_input,
                    msg.previous_network_output,
                    internal_reward,
                    msg.sensors_available,
                    msg.is_terminal,
                    msg.additional_data,
                )
            elif isinstance(msg, MuscleShutdownRequest):
                LOG.info(
                    "%s saw its only muscle requesting a break.",
                    self,
                )
                response = MuscleShutdownResponse()
                self._state = State.STOPPING
            else:
                LOG.error(
                    "%s has received a message of type %s, "
                    "but cannot handle it; ignoring",
                    self,
                    type(msg),
                )
                continue

            self._send(response, header)

        LOG.debug(
            "%s runner has ended, joining receiver thread",
            self,
        )
        receiver_thread.join()
        if self.state == State.STOPPING:
            self._state = State.FINISHED
        self.store()
        LOG.info("%s completed shutdown.", self)

    @abstractmethod
    def thinking(
        self,
        muscle_id,
        readings,
        actions,
        reward,
        next_state,
        done,
        additional_data,
    ) -> MuscleUpdateResponse:
        """Think about a response using the provided information.

        The :meth:`.thinking` method is the place for the
        implementation of the agent's/brain's logic. The brain can
        use the current sensor readings, review the actions of the
        previous thinking and consider the reward (provided by the
        objective).

        Usually, this is the place where machine learning happens,
        but other solutions are possible as well (like a set of rules
        or even random based results).

        Parameters
        -------
        muscle_id : UID
            This is the ID of the muscle which requested the update
        readings : list[sensor_information]
            A list containing the new sensor readings
        actions : list[actuator_information]
            A list containing the actions of the last iteration
        reward : list[floats] or list[int] or float
            A float of the environment reward, a list of floats if
            multiple environments are used in parallel
        next_state: list
            List of sensor readings
        done : bool
            A boolean which signals if the simulation run has
            terminated
        additional_data: dict
            A dictionary containing additional data to be
            exchanged between muscle and brain.

        Returns
        -------
        MuscleUpdateResponse
        """
        pass

    def store(self):
        """Stores the current state of the model

        This method is called whenever the current state of the brain should
        be saved. How a particular model is serialized is up to the concrete
        implementation. Also, brains may be divided into sub-models (e.g.,
        actor and critic), whose separate storage is relized via tags.
        Implementing this method allows for a versatile implementation of this.

        It is advisable to use the storage facilities of palaestrAI. They are
        available through the method
        :meth:`~BrainDumper.store_brain_dump(binary_io, self._dumpers, tag)`.
        This function calls all available dumpers to store the serialized
        brain dump provided in the parameter ``binary_io`` and optionally
        attaches a ``tag`` to it. The attribute ::`~Brain._dumpers` is
        initialized to a list of available dumpers and can be used directly.
        """
        try:
            # Try to be backwards compatible and call the old store_model
            # method as a default implementation
            if self._dumpers:
                locator = self._dumpers[0]._brain_destination
                path = (
                    Path(RuntimeConfig().data_path).resolve()
                    / "brains"
                    / locator.experiment_run_uid
                    / str(locator.experiment_run_phase)
                    / str(locator.agent_name)
                )
            else:
                path = RuntimeConfig().data_path
            path.mkdir(parents=True, exist_ok=True)
            self.store_model(path)
            LOG.warning(
                "%s uses deprecated storage API, please upgrade to "
                "store()/load().",
                self,
            )
        except Exception as e:
            # This okay, sorts of. It means that the brain does not implement
            # any way of storing its state, which might be okay as well...
            LOG.warning(
                "%s does not implement store(); its current state "
                "cannot be saved. Please provide an implementation of "
                "%s.store(). Providing an empty one silences this "
                "warning. (Error message was: %s)",
                self,
                self,
                e,
            )
            pass

    def load(self):
        """Loads the current state of the model

        This method is called whenever the current state of the brain should
        be restored. How a particular model is deserialized is up to the
        concrete implementation. Also, brains may be divided into sub-models
        (e.g., actor and critic), whose separate storage is relized via tags.
        Implementing this method allows for a versatile implementation of this.

        It is advisable to use the storage facilities of palaestrAI. They are
        available through the method
        :meth:`~BrainDumper.load_brain_dump(self._dumpers, tag)`.
        This function calls all available dumpers to restore the serialized
        brain dump (optionally identified via a ``tag``). It returns a
        BinaryIO object that can then be used in the implementation. The
        attribute ::`~Brain._dumpers` is initialized to the list of available
        dumpers/loaders.
        """
        try:
            # Try to be backwards compatible and call the old load_model
            # method as a default implementation
            if self._dumpers and self._dumpers[0]._brain_source:
                locator = self._dumpers[0]._brain_source
                path = (
                    Path(RuntimeConfig().data_path).resolve()
                    / "brains"
                    / locator.experiment_run_uid
                    / str(locator.experiment_run_phase)
                    / str(locator.agent_name)
                )
            else:
                path = RuntimeConfig().data_path
            self.load_model(path)
            LOG.warning(
                "%s uses deprecated storage API, please upgrade to "
                "store()/load().",
                self,
            )
        except Exception as e:
            # This okay, sorts of. It means that the brain does not implement
            # any way of storing its state, which might be okay as well...
            LOG.warning(
                "%s does not implement load(); its current state cannot"
                " be loaded. Please provide an implementation of "
                "%s.load(). Providing an empty one silences this "
                "warning. (Error message was: %s)",
                self,
                self,
                e,
            )
            pass

    def load_model(self, path):
        warnings.warn(
            "Brain.load_model is deprecated and will be removed in "
            "palaestrAI 4.0. Please use the new store()/load() "
            "infrastructure instead.",
            DeprecationWarning,
        )

    def store_model(self, path):
        warnings.warn(
            "Brain.store_model is deprecated and will be removed in "
            "palaestrAI 4.0. Please use the new store()/load() "
            "infrastructure instead.",
            DeprecationWarning,
        )

    def __str__(self):
        return "%s(id=0x%x, uid=%s)" % (self.__class__, id(self), self._uid)
