from __future__ import annotations

import io
import sys
from typing import TYPE_CHECKING, List, Union

from .brain import Brain
from .brain_dumper import BrainDumper
from ..core.protocol import MuscleUpdateResponse

if TYPE_CHECKING:
    import socket
    from .objective import Objective
    from .sensor_information import SensorInformation
    from .actuator_information import ActuatorInformation


class DummyBrain(Brain):
    def __init__(
        self,
        muscle_updates_listen_uri_or_socket: Union[str, socket.socket],
        sensors: List[SensorInformation],
        actuators: List[ActuatorInformation],
        objective: Objective,
        seed: int,
        **params,
    ):
        super().__init__(
            muscle_updates_listen_uri_or_socket,
            sensors,
            actuators,
            objective,
            seed,
            **params,
        )
        self._dummy_value = 42  # Our brain state. Designated by fair dice roll

    def thinking(
        self,
        muscle_id,
        readings,
        actions,
        reward,
        next_state,
        done,
        additional_data,
    ):
        response = MuscleUpdateResponse(False, None)
        return response

    def load(self):
        try:
            self._dummy_value = int.from_bytes(
                BrainDumper.load_brain_dump(self._dumpers).read(),
                sys.byteorder,
            )
        except AttributeError:
            # We returned "None"
            pass

    def store(self):
        bio = io.BytesIO(
            self._dummy_value.to_bytes(
                (self._dummy_value.bit_length() + 7) // 8, sys.byteorder
            )
        )
        BrainDumper.store_brain_dump(bio, self._dumpers)
