*** Settings ***
Documentation   Run palaestrAI from Jupyter Notebooks
...
...             The Jupyter Notebook kernel is a special environment for
...             palaestrAI to run it. This system test will run
...             jupyter nbconvert --execute for a given iPython notebook in
...             which a palaestrAI experiment is executed.

Library         Process
Library         OperatingSystem         
# Suite Teardown  Clean Files

*** Keywords ***
Clean Files
    Remove File                     ${TEMPDIR}${/}stdout.txt
    Remove File                     ${TEMPDIR}${/}stderr.txt

*** Test Cases ***
check existig palaestrai modules
    ${py_modules} =                 Run Process     pip  freeze
    Should Contain                  ${py_modules.stdout}   palaestrai
    Should Contain                  ${py_modules.stdout}   palaestrai-environments
    Should Contain                  ${py_modules.stdout}   harl
    Should Contain                  ${py_modules.stdout}   midas-palaestrai
    Should Contain                  ${py_modules.stdout}   midas-mosaik
    Should Contain                  ${py_modules.stdout}   midas-powergrid

midas medium voltage experiment run from a Jupyter Notebook
    START PROCESS                   jupyter  nbconvert    --to    html   --execute   ${CURDIR}${/}midas_integrationtest_palaestrai.ipynb    stdout=${TEMPDIR}${/}stdout.txt     stderr=${TEMPDIR}${/}stderr.txt  alias=arl-integrationtest
    ${result} =                     Wait For Process  handle=arl-integrationtest  timeout=300  on_timeout=kill
    Log Many                        ${result.stdout}    ${result.stderr}
    Should Be Equal As Integers     ${result.rc}   0
    File Should Exist               ${CURDIR}${/}midas_integrationtest_palaestrai.html

check the existance of agents brain files:
    FOR    ${phase}    IN RANGE    3
        #check attacker train phase brain files
        IF  ${phase} == 0 
            File Should Exist               ${CURDIR}${/}_outputs/brains/Classic-ARL-Experiment-0/${phase}/Sauron/ppo_actor
            File Should Exist               ${CURDIR}${/}_outputs/brains/Classic-ARL-Experiment-0/${phase}/Sauron/ppo_critic
        END
        #check defender train phase brain files
        IF  ${phase} == 1
            File Should Exist               ${CURDIR}${/}_outputs/brains/Classic-ARL-Experiment-0/${phase}/Gandalf/ddpg_actor
            File Should Exist               ${CURDIR}${/}_outputs/brains/Classic-ARL-Experiment-0/${phase}/Gandalf/ddpg_critic
            File Should Exist               ${CURDIR}${/}_outputs/brains/Classic-ARL-Experiment-0/${phase}/Gandalf/ddpg_target_actor
            File Should Exist               ${CURDIR}${/}_outputs/brains/Classic-ARL-Experiment-0/${phase}/Gandalf/ddpg_target_critic
        END
        # check pair attacker-defender agents train phase brain files
        IF  ${phase} == 2
            File Should Exist               ${CURDIR}${/}_outputs/brains/Classic-ARL-Experiment-0/${phase}/Sauron/ppo_actor
            File Should Exist               ${CURDIR}${/}_outputs/brains/Classic-ARL-Experiment-0/${phase}/Sauron/ppo_critic
            File Should Exist               ${CURDIR}${/}_outputs/brains/Classic-ARL-Experiment-0/${phase}/Gandalf/ddpg_actor
            File Should Exist               ${CURDIR}${/}_outputs/brains/Classic-ARL-Experiment-0/${phase}/Gandalf/ddpg_critic
            File Should Exist               ${CURDIR}${/}_outputs/brains/Classic-ARL-Experiment-0/${phase}/Gandalf/ddpg_target_actor
            File Should Exist               ${CURDIR}${/}_outputs/brains/Classic-ARL-Experiment-0/${phase}/Gandalf/ddpg_target_critic
        END
    END
    